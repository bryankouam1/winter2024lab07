public class SimpleWar{ 
	public static void main(String[]args){
	//My deck
	Deck myDeck = new Deck();
	myDeck.shuffle();
	//Players Points
	int playerPoints1=0;
	int playerPoints2=0;
	

	//Loop to keep score for players and points after each round
	while(myDeck.length()>0){
		double playerScore1=0;
		double playerScore2=0;
		
		Card card1=myDeck.drawTopCard();
		playerScore1=card1.calculateScore();
		System.out.println("Score for Player 1 is "+playerScore1+" with "+card1);
		
		Card card2=myDeck.drawTopCard();
		playerScore2=card2.calculateScore();
		System.out.println("Score is Player 2"+playerScore2+" with "+card2);
		
		System.out.println("********************************");
		if(card1.calculateScore() >card2.calculateScore()){
			System.out.println("Player 1 Wins with: "+playerScore1+" \nand Player 2 Loses with: "+playerScore2);
			playerPoints1++;
			System.out.println("Player 1 points :"+playerPoints1);
		}
		else{
			System.out.println("Player 2 Wins with: "+playerScore2+" \nand Player 1 Loses with: "+playerScore1);
			playerPoints2++;
			System.out.println("Player 2 points :"+playerPoints2);
		}
	
	}
	//Congratulation message to winner
	if(playerPoints1>playerPoints2){
		System.out.println("Congratulation to the winner: Player 1 with "+playerPoints1+"!!");
	}
	else{
		System.out.println("Congratulation to the winner: Player 2"+playerPoints2+"!!");
	}
}
	
}