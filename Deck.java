import java.util.Random;
public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	//getters
	public int getNumberOfCards(){
		return this.numberOfCards;
	}
	public Card[] getCards(){
		return this.cards;
	}

	//constructor
	public Deck(){
		this.numberOfCards=52;
		this.cards= new Card[numberOfCards];
		this.rng= new Random();
		
		String[] allSuits= {"Hearts","Diamonds","Spades","Clubs"};
		String[] allValues={"1","2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12","13"};
		int indexOfvalue=0;
		for(int j=0;j<allSuits.length;j++){
			
			for(int i=0;i<allValues.length;i++){
				cards[indexOfvalue]= new Card(allSuits[j],allValues[i]);
				indexOfvalue++;
			}
		}
	}
	//length method
	public int length(){
		return numberOfCards;
	}
	//drawing one card from deck method
	public Card drawTopCard(){
		numberOfCards--;
		return cards[numberOfCards];
	}
	//tostring method
	public String toString(){
		String deck="";
		for(int i=0;i<numberOfCards;i++){
			deck+= cards[i] +"\n";
		}
		return deck;
	}
	//shuffle the cards within the deck method
	public void shuffle(){
		
		for(int i=0;i<numberOfCards-1;i++){
			int randomNum = rng.nextInt(numberOfCards-1);
			Card temp = cards[i];
			this.cards[i]= cards[randomNum];
			cards[randomNum]= temp;
				
		}
	}
	
}